package com.aras.modir.volley.RecycleViewWithPicaso;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.aras.modir.volley.R;
import com.squareup.picasso.Picasso;

import static com.aras.modir.volley.RecycleViewWithPicaso.Main2Activity.EXTRA_Creator;
import static com.aras.modir.volley.RecycleViewWithPicaso.Main2Activity.EXTRA_LIKES;
import static com.aras.modir.volley.RecycleViewWithPicaso.Main2Activity.EXTRA_URL;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Intent intent = getIntent();
        String imageUrl = intent.getStringExtra(EXTRA_URL);
        String creatorName = intent.getStringExtra(EXTRA_Creator);
        int likeCount = intent.getIntExtra(EXTRA_LIKES, 0);

        ImageView imageView = findViewById(R.id.image_view_detail);
        TextView textViewCreator = findViewById(R.id.text_view_creator_detail);
        TextView textViewLikes = findViewById(R.id.text_view_like_detail);

        Picasso.get().load(imageUrl).fit().centerInside().into(imageView);
        textViewCreator.setText(creatorName);
        textViewLikes.setText("Like: " + likeCount);

    }
}
